package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import ru.ivarodsiy.mediadownloader.DefineExtension;
import ru.ivarodsiy.mediadownloader.Downloader;
import ru.ivarodsiy.mediadownloader.ExtractURL;
import ru.ivarodsiy.mediadownloader.RegexConstants;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Класс для работы с javafx приложением
 */
public class Controller {

    private ToggleGroup limitRadioGroup = new ToggleGroup();
    private static String mediaType = "music";

    @FXML
    private TextField urlTextField;
    @FXML
    private TextField limitTextField;
    @FXML
    private TextField pathTextField;
    @FXML
    private RadioButton radioMusic;
    @FXML
    private RadioButton radioImage;
    @FXML
    private ToggleGroup radioTypeGroup;
    @FXML
    private RadioButton noLimitRadioButton;
    @FXML
    private RadioButton limitRadioButton;
    @FXML
    private ToggleGroup radioLimitGroup;
    @FXML
    private Label needEnterPath;
    @FXML
    private Label needEnterLimit;
    @FXML
    private Label needEnterUrl;
    @FXML
    private Button downloadButton;

    /**
     * Событие - нажатие кнопки скачать
     * @param actionEvent - передаваемое действие
     */
    public void buttonClick(javafx.event.ActionEvent actionEvent) {
        List listUrl;
        String url = urlTextField.getCharacters().toString();
        String limit = limitTextField.getCharacters().toString();
        String path = pathTextField.getCharacters().toString();
        if (checkValidationURL() && checkValidationLimit() && checkValidationPath()) {
            listUrl = doExtractURL(url, limit);
            doDownload(listUrl,path);
        } else {
            return;
        }
    }

    /**
     * Вычленение ссылок на скачиване из HTML страницы
     * @param url - ссылка на сайт
     * @param limit - лимит скачивания
     * @return - список ссылок на скачивание
     */
    public List doExtractURL(String url, String limit) {
        Pattern pattern = null;
        List list = new ArrayList();
        try {
            ExtractURL extractURL = new ExtractURL(url);
            if (radioMusic.isSelected()) {
                pattern = Pattern.compile(RegexConstants.PATTERN_MUSIC);
            }
            if (radioImage.isSelected()) {
                pattern = Pattern.compile(RegexConstants.PATTERN_IMAGE);
            }

            if (limitRadioButton.isSelected()) {
                list = extractURL.extract(pattern, Integer.parseInt(limit));
            } else if (noLimitRadioButton.isSelected()) {
                list = extractURL.extract(pattern);
            }
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        } catch (RuntimeException e) {
        } catch (Exception e) {
        }
        return list;
    }

    /**
     * Скачивание файлов
     * @param listUrl - список ссылок для скачивания
     */
    public void doDownload(List listUrl, String path) {
        try {
            Downloader downloader = new Downloader(listUrl.get(0).toString(), path);
            for (int count = 0; count < listUrl.size(); count++) {
                DefineExtension defineExtension = new DefineExtension(mediaType, listUrl.get(count).toString());
                String extension = defineExtension.defineExtension();
                downloader.setUrl(listUrl.get(count).toString());
                downloader.setPath(path + "\\file_" + count + extension);
                downloader.download();
            }
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        } catch (RuntimeException e) {
        } catch (Exception e) {
        }
    }

    /**
     * Нажата радио кнопка безлимитного скачивания
     * @param actionEvent - передаваемое действие
     */
    public void radioNoLimitClick(javafx.event.ActionEvent actionEvent) {
        limitTextField.setText(null);
        limitTextField.setDisable(true);
    }

    /**
     * Нажата радио кнопка скачивания с лимитом
     * @param actionEvent - передаваемое действие
     */
    public void radioLimitClick(javafx.event.ActionEvent actionEvent) {
        limitTextField.setDisable(false);
    }

    /**
     * Нажата радио кнопка типа медиафайла - музыки
     * @param actionEvent - передаваемое действие
     */
    public void radioMusic(ActionEvent actionEvent) { mediaType = "music"; }

    /**
     * Нажата радио кнопка типа медиафайла - изображения
     * @param actionEvent - передаваемое действие
     */
    public void radioImage(ActionEvent actionEvent) { mediaType = "image"; }

    /**
     * Проверка на введенный URl
     * @return - результат проверки
     */
    public boolean checkValidationURL() {
        boolean correctness = true;
        String url = urlTextField.getCharacters().toString();
        if (url.matches(RegexConstants.PATTERN_URL)) {
            needEnterUrl.setText("*Введите ссылку на веб ресурс");
            correctness = false;
        }
        return correctness;
    }

    /**
     * Проверка на введенный лимит
     * @return - результат проверки
     */
    public boolean checkValidationLimit() {
        boolean correctness = true;
        String limit = limitTextField.getCharacters().toString();
        if (limitRadioButton.isSelected() && !limit.matches(RegexConstants.PATTERN_LIMIT)) {
            needEnterLimit.setText("*Введите значение лимита");
            correctness = false;
        }
        return correctness;
    }

    /**
     * Проверка на введенный путь
     * @return - результат проверки
     */
    public boolean checkValidationPath() {
        boolean correctness = true;
        String path = pathTextField.getCharacters().toString();
        if (!path.matches(RegexConstants.PATTERN_PATH)) {
            needEnterPath.setText("*Введите корректный путь сохранения");
            correctness = false;
        }
        return correctness;
    }

}
