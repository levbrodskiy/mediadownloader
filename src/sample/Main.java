package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Программа для скачивания медиафайлов с сайта
 * @author Лев Бродский Иванчин Егор 17ИТ18
 */
public class Main extends Application {
    /**
     * Запуск приложения
     * @param primaryStage
     * @throws Exception
     */
    @Override

    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Media Downloader");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);

    }
}
