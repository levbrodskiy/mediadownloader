package ru.ivarodsiy.mediadownloader;

public class DefineExtension {
    private String url;
    private String mediaType;

    public DefineExtension(String mediaType, String url) {
        this.mediaType = mediaType;
        this.url = url;
    }

    public String defineExtension() throws Exception {

        String str = (url);
        String extension;

        if (mediaType.equals("music")) {
            extension = ".mp3";
        } else if (mediaType.equals("image")) {
            String[] list;
            list = str.split("\\.");
            extension = "." + list[list.length - 1];
        } else {
            throw new Exception();
        }
        return extension;
    }
}
