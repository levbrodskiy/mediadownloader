package ru.ivarodsiy.mediadownloader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Загрузчик медиа файлов
 */
public class Downloader {
    private URL url;
    private String path;

    /**
     * Конструктор класса
     * @param url - ссылка на скачивание
     * @param path - путь сохранения файла
     * @throws MalformedURLException
     */
    public Downloader(String url, String path) throws MalformedURLException {
        this.url = new URL(url);
        this.path = path;
    }

    /**
     * Получение значения url
     * @return - значение url
     */
    public String getUrl() {
        return url.toString();
    }

    /**
     * Получение значения path
     * @return
     */
    public String getPath() {
        return path;
    }

    /**
     * Задание значения url
     * @param url - значение url
     * @throws MalformedURLException
     */
    public void setUrl(String url) throws MalformedURLException {
        this.url = new URL(url);
    }

    /**
     * Задание значения path
     * @param path - значение path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Скачивание файла
     * @throws Exception
     */
    public void download() throws IOException {

        FileOutputStream stream;
        try (ReadableByteChannel byteChannel = Channels.newChannel(url.openStream())) {
            stream = new FileOutputStream(new File(path));
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException();
        } catch (IOException e) {
            throw new IOException();
        }
    }

}