package ru.ivarodsiy.mediadownloader;

/**
 * Константы регулярных выражений
 */
public class RegexConstants {
    public static final String PATTERN_MUSIC = "\\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")";
    public static final String PATTERN_IMAGE = "https?://\\S+(?:jpg|jpeg|png)(?=\")";
    public static final String PATTERN_PATH = "[A-Z]:(\\\\[\\\\a-zA-Z0-9_]+)+";
    public static final String PATTERN_URL = "https?://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    public static final String PATTERN_LIMIT = "[0-9]+";
}
