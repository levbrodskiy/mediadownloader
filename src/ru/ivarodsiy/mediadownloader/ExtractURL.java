package ru.ivarodsiy.mediadownloader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Нахождение ссылок на скачивание нужных файлов
 */
public class ExtractURL {
    private URL url;

    /**
     * Конструктор Нахождения ссылок
     * @param url - ссылка на страницу
     * @throws MalformedURLException
     */
    public ExtractURL(String url) throws MalformedURLException {
        this.url = new URL(url);
    }

    /**
     * Получение значения url
     * @return - значение url
     */
    public String getUrl() {
        return url.toString();
    }

    /**
     * Задание значения url
     * @param url - значение url
     * @throws MalformedURLException
     */
    public void setUrl(String url) throws MalformedURLException {
        this.url = new URL(url);
    }

    /**
     * Вычленение ссылок из HTML странцы с определенным лимитом
     * @param pattern - паттерн для поиска ссылок
     * @param limit - лимит на количество ссылок
     * @return - список ссылок
     * @throws IOException
     */
    public List extract(Pattern pattern, int limit) throws IOException {
        List list = new ArrayList();
        int startLimit = 1;
        try  (BufferedReader buffer = new BufferedReader(new InputStreamReader(url.openStream())) ) {
            String pageLine;
            while (((pageLine = buffer.readLine()) != null)) {
                Matcher matcher = pattern.matcher(pageLine);
                while (matcher.find() &&(startLimit <= limit)){
                    list.add(matcher.group());
                    startLimit++;
                }
            }
        } catch (IOException e) {
            throw new IOException();
        }
        return list;
    }
    /**
     * Вычленение ссылок из HTML странцы без лимита
     * @param pattern - паттерн для поиска ссылок
     * @return - список ссылок
     * @throws IOException
     */
    public List extract(Pattern pattern) throws IOException {
        List list = new ArrayList();
        try  (BufferedReader buffer = new BufferedReader(new InputStreamReader(url.openStream())) ) {
            String pageLine;

            while ((pageLine = buffer.readLine()) != null) {
                Matcher matcher = pattern.matcher(pageLine);
                while (matcher.find()){
                    list.add(matcher.group());
                }
            }
        } catch (IOException e) {
            throw new IOException();
        }
        return list;
    }
}

